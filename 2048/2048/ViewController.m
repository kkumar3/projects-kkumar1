//
//  ViewController.m
//  2048
//
//  Created by karan on 2/2/17.
//  Copyright © 2017 karan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *scoreboard;

@end





@implementation ViewController

int score=0;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(int)getRandom:(int)from to:(int)to{
    return (int)from + arc4random()%(to-from+1);
}

- (IBAction)uppress:(id)sender {                                    //Action function for UP button
  
    BOOL flag = 0;                                                  //flag to record a successful cell addition
    for(int k=0; k< 4; k++){                                        //first for to iterate over 4 columns
        flag = 0;
        for(int i=12+k; i>k;){                                          //2nd for loop which iterates over each cell in column
            
            if([((UILabel*)[self.cubeArray objectAtIndex:i]).text isEqualToString:((UILabel*)[self.cubeArray objectAtIndex:i-4]).text] && !flag)                   // if adjacent cell have same values
            {
                flag = 1;
                
                int newval = [((UILabel*)[self.cubeArray objectAtIndex:i]).text intValue] + [((UILabel*)[self.cubeArray objectAtIndex:i-4]).text intValue];                 //add the cell values and store in a varibale
                if(newval == 0){
                   ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
                }
                
                else{
                    ((UILabel*)[self.cubeArray objectAtIndex:i-4]).text = [NSString stringWithFormat:@"%i",newval];
                    ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];    //Store the added value to the corresponding cell and set the other as null
                    score=score+1;
                    self.scoreboard.text = [NSString stringWithFormat:@"%i",score];                     //increment and update score
                    
                }
            }
    
      
            if([((UILabel*)[self.cubeArray objectAtIndex:i-4]).text isEqualToString:@""])              //to move cells and cover empty cells
            {
                ((UILabel*)[self.cubeArray objectAtIndex:i-4]).text = ((UILabel*)[self.cubeArray objectAtIndex:i]).text;
                
                ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
            }
        
            i=i-4;
        }
    }
    BOOL randomnumFlag = 0;
    
    int randomnum = [self getRandom:0 to:15];                                  //random function to get a random value b/w 0 and 15
    int runcount = 0;
    
    
    while(randomnumFlag == 0 && runcount <16){                              //place '2' in a random cell
        
       
        if([((UILabel*)[self.cubeArray objectAtIndex:randomnum]).text isEqualToString:@""]){
            ((UILabel*)[self.cubeArray objectAtIndex:randomnum]).text = [NSString stringWithFormat:@"2"];
            randomnumFlag=1;
            }
        runcount++;
    }

}

- (IBAction)downpress:(id)sender {                                         //Action function for down button
    BOOL flag = 0;
    for(int k=0; k< 4; k++){
        flag = 0;
        for(int i=k; i<12+k;){
           
            
            if([((UILabel*)[self.cubeArray objectAtIndex:i]).text isEqualToString:((UILabel*)[self.cubeArray objectAtIndex:i+4]).text] && !flag)
            {
                flag = 1;
              
                int newval = [((UILabel*)[self.cubeArray objectAtIndex:i]).text intValue] + [((UILabel*)[self.cubeArray objectAtIndex:i+4]).text intValue];
                
                if(newval == 0){
                    ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
                }
                
                else{
                    
                    ((UILabel*)[self.cubeArray objectAtIndex:i+4]).text = [NSString stringWithFormat:@"%i",newval];
                    ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
                    
                    score=score+1;
                    self.scoreboard.text = [NSString stringWithFormat:@"%i",score];
                }
            }
            
            
            if([((UILabel*)[self.cubeArray objectAtIndex:i+4]).text isEqualToString:@""])
            {
                ((UILabel*)[self.cubeArray objectAtIndex:i+4]).text = ((UILabel*)[self.cubeArray objectAtIndex:i]).text;
                
                ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
            }
            
            i=i+4;
        }
    }
    
    BOOL randomnumFlag = 0;
    
    int randomnum = [self getRandom:0 to:15];
    int runcount = 0;
    
    while(randomnumFlag == 0 && runcount <16){
        
        if([((UILabel*)[self.cubeArray objectAtIndex:randomnum]).text isEqualToString:@""]){
            ((UILabel*)[self.cubeArray objectAtIndex:randomnum]).text = [NSString stringWithFormat:@"2"];
            randomnumFlag=1;
        }
        runcount++;
    }
    
    
}

- (IBAction)leftpress:(id)sender {                                      //Action button for left button
    BOOL flag = 0;
    for(int k=0; k< 16; k=k+4){
        flag = 0;
        for(int i=3+k; i>k; i--){
            
            
            if([((UILabel*)[self.cubeArray objectAtIndex:i]).text isEqualToString:((UILabel*)[self.cubeArray objectAtIndex:i-1]).text] && !flag)
            {
                flag = 1;
               
                int newval = [((UILabel*)[self.cubeArray objectAtIndex:i]).text intValue] + [((UILabel*)[self.cubeArray objectAtIndex:i-1]).text intValue];
                
                if(newval == 0){
                    ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
                }
                
                else{
                    
                    ((UILabel*)[self.cubeArray objectAtIndex:i-1]).text = [NSString stringWithFormat:@"%i",newval];
                    ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
                    
                    score=score+1;
                    self.scoreboard.text = [NSString stringWithFormat:@"%i",score];
                }
            }
            
            
            if([((UILabel*)[self.cubeArray objectAtIndex:i-1]).text isEqualToString:@""])
            {
                ((UILabel*)[self.cubeArray objectAtIndex:i-1]).text = ((UILabel*)[self.cubeArray objectAtIndex:i]).text;
                
                ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
            }
            
            
        }
    }
    
    BOOL randomnumFlag = 0;
    
    int randomnum = [self getRandom:0 to:15];
    int runcount = 0;
    
    while(randomnumFlag == 0 && runcount <16){
        if([((UILabel*)[self.cubeArray objectAtIndex:randomnum]).text isEqualToString:@""]){
            ((UILabel*)[self.cubeArray objectAtIndex:randomnum]).text = [NSString stringWithFormat:@"2"];
            randomnumFlag=1;
        }
        runcount++;
    }

}

- (IBAction)rightpress:(id)sender {                                 //Action function for right button
    
    BOOL flag = 0;
    for(int k=0; k< 16; k=k+4){
        flag = 0;
        for(int i=k; i<k+3	; i++){
           
            if([((UILabel*)[self.cubeArray objectAtIndex:i]).text isEqualToString:((UILabel*)[self.cubeArray objectAtIndex:i+1]).text] && !flag)
            {
                flag = 1;
               
                int newval = [((UILabel*)[self.cubeArray objectAtIndex:i]).text intValue] + [((UILabel*)[self.cubeArray objectAtIndex:i+1]).text intValue];
                
                if(newval == 0){
                    ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
                }
                
                else{
                    
                    ((UILabel*)[self.cubeArray objectAtIndex:i+1]).text = [NSString stringWithFormat:@"%i",newval];
                    ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
                }
            }
            
            
            if([((UILabel*)[self.cubeArray objectAtIndex:i+1]).text isEqualToString:@""])
            {
                ((UILabel*)[self.cubeArray objectAtIndex:i+1]).text = ((UILabel*)[self.cubeArray objectAtIndex:i]).text;
                
                ((UILabel*)[self.cubeArray objectAtIndex:i]).text = [NSString stringWithFormat:@""];
            }
            
            
        }
    }
    
    BOOL randomnumFlag = 0;
    
    int randomnum = [self getRandom:0 to:15];
    int runcount = 0;
    
    while(randomnumFlag == 0 && runcount <16){
        
        if([((UILabel*)[self.cubeArray objectAtIndex:randomnum]).text isEqualToString:@""]){
            ((UILabel*)[self.cubeArray objectAtIndex:randomnum]).text = [NSString stringWithFormat:@"2"];
            randomnumFlag=1;
        }
        runcount++;
    }
 
    
}

@end
