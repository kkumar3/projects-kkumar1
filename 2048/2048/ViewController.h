//
//  ViewController.h
//  2048
//
//  Created by karan on 2/2/17.
//  Copyright © 2017 karan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property 	(retain,nonatomic) IBOutletCollection(UILabel) NSMutableArray * cubeArray;                  //Mutable array to store the Label objects.
-(int)getRandom:(int)from to:(int)to;

@end
	
