//
//  ViewController.m
//  proj1
//
//  Created by karan on 1/31/17.
//  Copyright © 2017 karan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *messagelabel;
@property (weak, nonatomic) IBOutlet UITextField *answer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (IBAction)Submitpressed:(id)sender {
    
    if ([self.answer isEqual: 0]) {
        [self.messagelabel setText:@"Well you are genius"];
    }
    else{
            [self.messagelabel setText:@"Congrats, that was a wrong answer and you are obviously a human"];
        }
    
    
}

@end
