//
//  AppDelegate.h
//  proj1
//
//  Created by karan on 1/31/17.
//  Copyright © 2017 karan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

